CREATE TABLE `wp_wlm_contentlevel_options` (  `ID` bigint(20) NOT NULL AUTO_INCREMENT,  `contentlevel_id` bigint(20) NOT NULL,  `option_name` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',  PRIMARY KEY (`ID`),  UNIQUE KEY `contentlevel_id` (`contentlevel_id`,`option_name`),  KEY `autoload` (`autoload`),  KEY `contentlevel_id2` (`contentlevel_id`),  KEY `option_name` (`option_name`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `wp_wlm_contentlevel_options` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_wlm_contentlevel_options` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
