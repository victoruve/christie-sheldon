CREATE TABLE `wp_groups_group` (  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `parent_id` bigint(20) DEFAULT NULL,  `creator_id` bigint(20) DEFAULT NULL,  `datetime` datetime DEFAULT NULL,  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,  `description` longtext COLLATE utf8mb4_unicode_ci,  PRIMARY KEY (`group_id`),  UNIQUE KEY `group_n` (`name`)) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_groups_group` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wp_groups_group` VALUES('1', NULL, '1', '2018-07-06 22:34:18', 'Registered', NULL);
/*!40000 ALTER TABLE `wp_groups_group` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
