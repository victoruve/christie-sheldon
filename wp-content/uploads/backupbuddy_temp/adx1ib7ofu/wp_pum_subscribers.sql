CREATE TABLE `wp_pum_subscribers` (  `ID` bigint(20) NOT NULL AUTO_INCREMENT,  `email_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `popup_id` bigint(20) NOT NULL,  `user_id` bigint(20) NOT NULL,  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `values` longtext COLLATE utf8mb4_unicode_ci NOT NULL,  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `created` datetime NOT NULL,  PRIMARY KEY (`ID`),  KEY `email` (`email`(191)),  KEY `user_id` (`user_id`),  KEY `popup_id` (`popup_id`),  KEY `email_hash` (`email_hash`(191))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_pum_subscribers` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_pum_subscribers` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
