CREATE TABLE `wp_wpam_actions` (  `actionId` int(11) NOT NULL AUTO_INCREMENT,  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  PRIMARY KEY (`actionId`)) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_wpam_actions` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wp_wpam_actions` VALUES('1', 'visit', 'New visitor');
INSERT INTO `wp_wpam_actions` VALUES('2', 'purchase', 'User confirmed purchase');
/*!40000 ALTER TABLE `wp_wpam_actions` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
