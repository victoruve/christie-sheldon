CREATE TABLE `wp_social_users` (  `ID` int(11) NOT NULL,  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,  `identifier` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,  KEY `ID` (`ID`,`type`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_social_users` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_social_users` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
