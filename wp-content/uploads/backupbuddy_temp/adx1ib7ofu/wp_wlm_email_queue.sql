CREATE TABLE `wp_wlm_email_queue` (  `id` bigint(20) NOT NULL AUTO_INCREMENT,  `broadcastid` int(9) NOT NULL,  `userid` bigint(20) NOT NULL,  `failed` int(1) NOT NULL DEFAULT '0',  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  UNIQUE KEY `id` (`id`),  KEY `broadcastid` (`broadcastid`),  CONSTRAINT `wpjj_wlm_email_queue_ibfk_1` FOREIGN KEY (`broadcastid`) REFERENCES `wp_wlm_emailbroadcast` (`id`) ON DELETE CASCADE ON UPDATE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `wp_wlm_email_queue` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_wlm_email_queue` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
