CREATE TABLE `wp_pmpro_membership_levelmeta` (  `meta_id` int(10) unsigned NOT NULL AUTO_INCREMENT,  `pmpro_membership_level_id` int(10) unsigned NOT NULL,  `meta_key` varchar(255) NOT NULL,  `meta_value` longtext,  PRIMARY KEY (`meta_id`),  KEY `pmpro_membership_level_id` (`pmpro_membership_level_id`),  KEY `meta_key` (`meta_key`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40000 ALTER TABLE `wp_pmpro_membership_levelmeta` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_pmpro_membership_levelmeta` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
