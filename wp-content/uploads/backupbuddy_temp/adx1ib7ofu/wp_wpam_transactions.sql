CREATE TABLE `wp_wpam_transactions` (  `transactionId` int(11) NOT NULL AUTO_INCREMENT,  `dateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,  `dateCreated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',  `affiliateId` int(11) NOT NULL,  `amount` decimal(18,2) NOT NULL,  `type` enum('credit','payout','adjustment','refund') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'credit',  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `referenceId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `status` enum('pending','confirmed','failed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'confirmed',  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  PRIMARY KEY (`transactionId`)) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_wpam_transactions` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wp_wpam_transactions` VALUES('1', '0000-00-00 00:00:00', '2018-03-29 14:48:45', '1', '10.00', 'adjustment', '', NULL, 'confirmed', '');
INSERT INTO `wp_wpam_transactions` VALUES('2', '0000-00-00 00:00:00', '2018-03-29 14:49:11', '1', '-10.00', 'payout', 'Payout', NULL, 'confirmed', '');
/*!40000 ALTER TABLE `wp_wpam_transactions` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
