CREATE TABLE `wp_wpam_paypal_logs` (  `paypalLogId` int(11) NOT NULL AUTO_INCREMENT,  `responseTimestamp` datetime NOT NULL,  `dateOccurred` datetime NOT NULL,  `correlationId` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,  `ack` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,  `version` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,  `build` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,  `errors` text COLLATE utf8mb4_unicode_ci NOT NULL,  `rawResponse` text COLLATE utf8mb4_unicode_ci NOT NULL,  `status` enum('pending','reconciled','failed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',  `amount` decimal(18,2) NOT NULL,  `fee` decimal(18,2) NOT NULL,  `totalAmount` decimal(18,2) NOT NULL,  PRIMARY KEY (`paypalLogId`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_wpam_paypal_logs` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_wpam_paypal_logs` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
