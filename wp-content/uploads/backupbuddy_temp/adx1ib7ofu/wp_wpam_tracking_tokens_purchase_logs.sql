CREATE TABLE `wp_wpam_tracking_tokens_purchase_logs` (  `trackingTokenPurchaseLogId` int(11) NOT NULL AUTO_INCREMENT,  `trackingTokenId` int(11) NOT NULL,  `purchaseLogId` int(11) NOT NULL,  PRIMARY KEY (`trackingTokenPurchaseLogId`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_wpam_tracking_tokens_purchase_logs` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_wpam_tracking_tokens_purchase_logs` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
