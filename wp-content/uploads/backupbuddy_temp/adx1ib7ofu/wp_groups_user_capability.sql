CREATE TABLE `wp_groups_user_capability` (  `user_id` bigint(20) unsigned NOT NULL,  `capability_id` bigint(20) unsigned NOT NULL,  PRIMARY KEY (`user_id`,`capability_id`),  KEY `user_capability_cu` (`capability_id`,`user_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_groups_user_capability` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_groups_user_capability` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
