CREATE TABLE `wp_wlm_api_queue` (  `ID` bigint(20) NOT NULL AUTO_INCREMENT,  `name` varchar(64) CHARACTER SET latin1 NOT NULL,  `value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,  `notes` varchar(500) CHARACTER SET latin1 DEFAULT NULL,  `tries` int(11) NOT NULL,  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  PRIMARY KEY (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `wp_wlm_api_queue` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_wlm_api_queue` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
