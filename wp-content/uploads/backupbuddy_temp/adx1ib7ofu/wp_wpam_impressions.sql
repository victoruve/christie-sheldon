CREATE TABLE `wp_wpam_impressions` (  `impressionId` int(11) NOT NULL AUTO_INCREMENT,  `dateCreated` datetime NOT NULL,  `sourceAffiliateId` int(11) NOT NULL,  `sourceCreativeId` int(11) DEFAULT NULL,  `referer` text COLLATE utf8mb4_unicode_ci,  `affiliateSubCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  PRIMARY KEY (`impressionId`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_wpam_impressions` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_wpam_impressions` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
