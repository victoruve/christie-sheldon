CREATE TABLE `wp_layerslider_revisions` (  `id` int(10) NOT NULL AUTO_INCREMENT,  `slider_id` int(10) NOT NULL,  `author` int(10) NOT NULL DEFAULT '0',  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,  `date_c` int(10) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_layerslider_revisions` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_layerslider_revisions` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
