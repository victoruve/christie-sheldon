CREATE TABLE `wp_wpam_creatives` (  `creativeId` int(10) unsigned NOT NULL AUTO_INCREMENT,  `type` enum('text','image') COLLATE utf8mb4_unicode_ci NOT NULL,  `altText` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `imagePostId` int(11) DEFAULT NULL,  `dateCreated` datetime NOT NULL,  `status` enum('active','inactive','deleted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,  `linkText` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  PRIMARY KEY (`creativeId`)) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_wpam_creatives` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wp_wpam_creatives` VALUES('1', 'text', '', NULL, '2018-03-29 14:37:43', 'active', 'default creative', 'default affiliate link', '');
/*!40000 ALTER TABLE `wp_wpam_creatives` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
