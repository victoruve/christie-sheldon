CREATE TABLE `wp_ai1ec_event_category_meta` (  `term_id` bigint(20) NOT NULL,  `term_color` varchar(255) NOT NULL,  `term_image` varchar(254) DEFAULT NULL,  PRIMARY KEY (`term_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `wp_ai1ec_event_category_meta` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_ai1ec_event_category_meta` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
