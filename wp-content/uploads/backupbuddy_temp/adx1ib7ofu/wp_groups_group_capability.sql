CREATE TABLE `wp_groups_group_capability` (  `group_id` bigint(20) unsigned NOT NULL,  `capability_id` bigint(20) unsigned NOT NULL,  PRIMARY KEY (`group_id`,`capability_id`),  KEY `group_capability_cg` (`capability_id`,`group_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_groups_group_capability` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_groups_group_capability` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
