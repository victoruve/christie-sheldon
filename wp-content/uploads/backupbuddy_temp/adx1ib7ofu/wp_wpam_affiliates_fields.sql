CREATE TABLE `wp_wpam_affiliates_fields` (  `affiliateFieldId` int(11) NOT NULL AUTO_INCREMENT,  `type` enum('base','custom') COLLATE utf8mb4_unicode_ci NOT NULL,  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,  `length` int(11) NOT NULL,  `fieldType` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,  `required` tinyint(1) NOT NULL,  `databaseField` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `enabled` tinyint(1) NOT NULL DEFAULT '0',  `order` int(11) NOT NULL,  PRIMARY KEY (`affiliateFieldId`)) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_wpam_affiliates_fields` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wp_wpam_affiliates_fields` VALUES('1', 'base', 'First Name', '50', 'string', '1', 'firstName', '1', '0');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('2', 'base', 'Last Name', '50', 'string', '1', 'lastName', '1', '1');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('3', 'base', 'E-Mail Address', '0', 'email', '1', 'email', '1', '3');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('4', 'base', 'Address Line 1', '255', 'string', '1', 'addressLine1', '1', '4');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('5', 'base', 'Address Line 2', '255', 'string', '0', 'addressLine2', '1', '5');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('6', 'base', 'City', '128', 'string', '1', 'addressCity', '1', '6');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('7', 'base', 'State', '0', 'stateCode', '1', 'addressState', '1', '7');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('8', 'base', 'Zip Code', '0', 'zipCode', '1', 'addressZipCode', '1', '8');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('9', 'base', 'Country', '0', 'countryCode', '1', 'addressCountry', '1', '9');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('10', 'base', 'Company Name', '50', 'string', '0', 'companyName', '1', '10');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('11', 'base', 'Website URL', '255', 'string', '0', 'websiteUrl', '1', '11');
INSERT INTO `wp_wpam_affiliates_fields` VALUES('12', 'base', 'Phone Number', '0', 'phoneNumber', '1', 'phoneNumber', '1', '2');
/*!40000 ALTER TABLE `wp_wpam_affiliates_fields` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
