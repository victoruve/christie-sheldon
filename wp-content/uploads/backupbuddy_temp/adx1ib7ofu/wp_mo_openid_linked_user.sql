CREATE TABLE `wp_mo_openid_linked_user` (  `id` mediumint(9) NOT NULL AUTO_INCREMENT,  `linked_social_app` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL,  `linked_email` varchar(55) COLLATE utf8mb4_unicode_520_ci NOT NULL,  `user_id` mediumint(10) NOT NULL,  `identifier` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `wp_mo_openid_linked_user` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_mo_openid_linked_user` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
