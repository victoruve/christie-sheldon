CREATE TABLE `wp_wpam_tracking_tokens` (  `trackingTokenId` int(11) NOT NULL AUTO_INCREMENT,  `dateCreated` datetime NOT NULL,  `sourceAffiliateId` int(11) NOT NULL,  `trackingKey` varchar(27) COLLATE utf8mb4_unicode_ci NOT NULL,  `sourceCreativeId` int(11) DEFAULT NULL,  `referer` text COLLATE utf8mb4_unicode_ci,  `affiliateSubCode` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `browser` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT '',  `ipAddress` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT '',  `customId` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT '',  PRIMARY KEY (`trackingTokenId`)) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_wpam_tracking_tokens` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wp_wpam_tracking_tokens` VALUES('1', '2018-03-29 14:49:37', '1', '5abcfd011fba0', '0', '', '', '', '67.167.192.225', '');
/*!40000 ALTER TABLE `wp_wpam_tracking_tokens` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
