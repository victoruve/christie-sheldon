CREATE TABLE `wp_wlm_emailbroadcast` (  `id` int(9) NOT NULL AUTO_INCREMENT,  `subject` varchar(400) COLLATE utf8mb4_unicode_520_ci NOT NULL,  `text_body` text COLLATE utf8mb4_unicode_520_ci NOT NULL,  `footer` text COLLATE utf8mb4_unicode_520_ci,  `send_to` varchar(15) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,  `mlevel` text COLLATE utf8mb4_unicode_520_ci NOT NULL,  `sent_as` varchar(5) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,  `status` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Queueing',  `otheroptions` text COLLATE utf8mb4_unicode_520_ci,  `total_queued` int(11) DEFAULT '0',  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  `from_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,  `from_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,  PRIMARY KEY (`id`),  UNIQUE KEY `id` (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40000 ALTER TABLE `wp_wlm_emailbroadcast` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_wlm_emailbroadcast` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
