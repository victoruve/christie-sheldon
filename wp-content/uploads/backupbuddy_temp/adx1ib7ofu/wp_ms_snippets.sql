CREATE TABLE `wp_ms_snippets` (  `id` bigint(20) NOT NULL AUTO_INCREMENT,  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,  `code` longtext COLLATE utf8mb4_unicode_ci NOT NULL,  `tags` longtext COLLATE utf8mb4_unicode_ci NOT NULL,  `scope` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'global',  `priority` smallint(6) NOT NULL DEFAULT '10',  `active` tinyint(1) NOT NULL DEFAULT '0',  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_ms_snippets` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `wp_ms_snippets` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
