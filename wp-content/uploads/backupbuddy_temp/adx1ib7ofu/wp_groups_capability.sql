CREATE TABLE `wp_groups_capability` (  `capability_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `capability` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,  `class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `object` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `description` longtext COLLATE utf8mb4_unicode_ci,  PRIMARY KEY (`capability_id`),  UNIQUE KEY `capability` (`capability`(100)),  KEY `capability_kco` (`capability`(20),`class`(20),`object`(20))) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_groups_capability` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wp_groups_capability` VALUES('1', 'switch_themes', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('2', 'edit_themes', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('3', 'activate_plugins', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('4', 'edit_plugins', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('5', 'edit_users', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('6', 'edit_files', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('7', 'manage_options', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('8', 'moderate_comments', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('9', 'manage_categories', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('10', 'manage_links', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('11', 'upload_files', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('12', 'import', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('13', 'unfiltered_html', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('14', 'edit_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('15', 'edit_others_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('16', 'edit_published_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('17', 'publish_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('18', 'edit_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('19', 'read', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('20', 'level_10', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('21', 'level_9', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('22', 'level_8', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('23', 'level_7', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('24', 'level_6', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('25', 'level_5', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('26', 'level_4', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('27', 'level_3', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('28', 'level_2', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('29', 'level_1', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('30', 'level_0', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('31', 'edit_others_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('32', 'edit_published_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('33', 'publish_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('34', 'delete_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('35', 'delete_others_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('36', 'delete_published_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('37', 'delete_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('38', 'delete_others_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('39', 'delete_published_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('40', 'delete_private_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('41', 'edit_private_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('42', 'read_private_posts', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('43', 'delete_private_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('44', 'edit_private_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('45', 'read_private_pages', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('46', 'delete_users', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('47', 'create_users', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('48', 'unfiltered_upload', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('49', 'edit_dashboard', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('50', 'update_plugins', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('51', 'delete_plugins', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('52', 'install_plugins', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('53', 'update_themes', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('54', 'install_themes', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('55', 'update_core', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('56', 'list_users', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('57', 'remove_users', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('58', 'promote_users', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('59', 'edit_theme_options', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('60', 'delete_themes', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('61', 'export', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('62', 'manage_woocommerce', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('63', 'view_woocommerce_reports', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('64', 'edit_product', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('65', 'read_product', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('66', 'delete_product', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('67', 'edit_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('68', 'edit_others_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('69', 'publish_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('70', 'read_private_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('71', 'delete_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('72', 'delete_private_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('73', 'delete_published_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('74', 'delete_others_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('75', 'edit_private_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('76', 'edit_published_products', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('77', 'manage_product_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('78', 'edit_product_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('79', 'delete_product_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('80', 'assign_product_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('81', 'edit_shop_order', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('82', 'read_shop_order', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('83', 'delete_shop_order', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('84', 'edit_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('85', 'edit_others_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('86', 'publish_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('87', 'read_private_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('88', 'delete_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('89', 'delete_private_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('90', 'delete_published_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('91', 'delete_others_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('92', 'edit_private_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('93', 'edit_published_shop_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('94', 'manage_shop_order_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('95', 'edit_shop_order_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('96', 'delete_shop_order_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('97', 'assign_shop_order_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('98', 'edit_shop_coupon', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('99', 'read_shop_coupon', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('100', 'delete_shop_coupon', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('101', 'edit_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('102', 'edit_others_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('103', 'publish_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('104', 'read_private_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('105', 'delete_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('106', 'delete_private_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('107', 'delete_published_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('108', 'delete_others_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('109', 'edit_private_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('110', 'edit_published_shop_coupons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('111', 'manage_shop_coupon_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('112', 'edit_shop_coupon_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('113', 'delete_shop_coupon_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('114', 'assign_shop_coupon_terms', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('115', 'manage_capabilities', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('116', 'aiosp_manage_seo', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('117', 'wpam_admin', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('118', 'pmpro_memberships_menu', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('119', 'pmpro_membershiplevels', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('120', 'pmpro_edit_memberships', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('121', 'pmpro_pagesettings', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('122', 'pmpro_paymentsettings', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('123', 'pmpro_emailsettings', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('124', 'pmpro_advancedsettings', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('125', 'pmpro_addons', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('126', 'pmpro_memberslist', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('127', 'pmpro_memberslistcsv', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('128', 'pmpro_reports', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('129', 'pmpro_orders', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('130', 'pmpro_orderscsv', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('131', 'pmpro_discountcodes', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('132', 'pmpro_updates', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('133', 'read_ai1ec_event', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('134', 'edit_ai1ec_event', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('135', 'edit_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('136', 'edit_private_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('137', 'edit_published_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('138', 'delete_ai1ec_event', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('139', 'delete_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('140', 'delete_published_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('141', 'delete_private_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('142', 'publish_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('143', 'read_private_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('144', 'manage_events_categories', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('145', 'manage_ai1ec_feeds', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('146', 'switch_ai1ec_themes', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('147', 'manage_ai1ec_options', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('148', 'edit_others_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('149', 'delete_others_ai1ec_events', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('150', 'Nginx Helper | Config', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('151', 'Nginx Helper | Purge cache', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('152', 'groups_access', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('153', 'groups_admin_groups', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('154', 'groups_admin_options', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('155', 'groups_restrict_access', NULL, NULL, NULL, NULL);
INSERT INTO `wp_groups_capability` VALUES('156', 'read_ai1ec_events', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `wp_groups_capability` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
