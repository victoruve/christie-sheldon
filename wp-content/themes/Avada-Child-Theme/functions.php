<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );


function register_intuition_menu () {
	register_nav_menu ('intuition-header-menu', __ ('Intuition Header Menu'));
}
add_action('init', 'register_intuition_menu');

function my_custom_members_area_sections1( $sections ) {
    $sections['impactwelcome'] = __( 'Welcome to I.M.P.A.C.T.', 'my-textdomain' );
    return $sections;
}
add_filter( 'wc_membership_plan_members_area_sections', 'my_custom_members_area_sections1' );

function my_custom_members_area_sections2( $sections ) {
    $sections['magicwelcome'] = __( 'Welcome to Magic', 'my-textdomain' );
    return $sections;
}
add_filter( 'wc_membership_plan_members_area_sections', 'my_custom_members_area_sections2' );

function my_custom_members_area_sections3( $sections ) {
    $sections['101welcome'] = __( 'Welcome to 101 ', 'my-textdomain' );
    return $sections;
}
add_filter( 'wc_membership_plan_members_area_sections', 'my_custom_members_area_sections3' );

function my_custom_members_area_sections4( $sections ) {
    $sections['201welcome'] = __( 'Welcome to 201', 'my-textdomain' );
    return $sections;
}
add_filter( 'wc_membership_plan_members_area_sections', 'my_custom_members_area_sections4' );

function my_custom_members_area_sections5( $sections ) {
    $sections['intuition101welcome2018'] = __( 'Welcome to 101 - 2018', 'my-textdomain' );
    return $sections;
}
add_filter( 'wc_membership_plan_members_area_sections', 'my_custom_members_area_sections5' );

// remove version from scripts and styles
function remove_version_scripts_styles($src) {
    if (strpos($src, 'ver=')) {
        $src = remove_query_arg('ver', $src);
    }
    return $src;
}
add_filter('style_loader_src', 'remove_version_scripts_styles', 9999);

?>





