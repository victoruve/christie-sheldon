<?php
/**
* Template Name: New Login Page Template
*/
?>
<?php get_header(); ?>
<style>
.shop_table td {
    padding: 15px 10px;
	font-size:18px;
}
.shop_table a.button.view {
    background-color: #67b7e1;
    display: inline-block;
    padding: 5px 25px;
    color: #fff;
    float: right;
}
.shop_table tr:nth-child(even) {background: #ffffff;}
.shop_table tr:nth-child(odd) {background: #fafafa;}
.woocommerce-MyAccount-content {
    border: none;
    background-color: #dddddd;
	padding-top:0px;
}
</style>
<?php
if ( is_user_logged_in() ) { ?>
   <?php echo do_shortcode('[woocommerce_my_account]'); ?>
	<?php the_content(); ?>
<?php } else { ?>
    <?php echo do_shortcode('[woocommerce_my_account]'); ?>
	<?php the_content(); ?>
<?php }
?>

<?php get_footer(); ?>