<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );


function register_intuition_menu () {
	register_nav_menu ('intuition-header-menu', __ ('Intuition Header Menu'));
}
add_action('init', 'register_intuition_menu');
/*
if ( ! function_exists( 'avada_header_1' ) ) {
	function avada_header_1() {
		if ( ! in_array( Avada()->settings->get( 'header_layout' ), array( 'v1', 'v2', 'v3' ) ) ) {
			return;
		}
		get_template_part( 'templates/header-1' );
	}
}
add_action( 'avada_header', 'avada_header_1', 20 );*/