<div class="fusion-header">
	<div class="fusion-row">
		<?php avada_logo(); ?>
		<?php if ( is_user_logged_in() ) {
		$main_menu_args = array(
			//'theme_location'  => 'intuition-header-menu',
			'theme_location' => 'intuition-header-menu',

			'depth'           => 5,
			'menu_class'      => 'fusion-menu',
			'items_wrap'      => '<ul role="menubar" id="%1$s" class="%2$s">%3$s</ul>',
			'container'       => false,
			'item_spacing'    => 'discard',
			'echo'            => false,
		);

		wp_nav_menu($main_menu_args);

	   	} else {
		 	avada_main_menu();
		}
		?>
	</div>
</div>
