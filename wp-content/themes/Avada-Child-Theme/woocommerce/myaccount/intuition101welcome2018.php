<style>
	.custom-woocomerce-page-v .welcome-section-v a {
		display:block; background-color:#4285F4; -moz-transition: background-color 0.5s ease, -moz-transform 0.5s ease; -webkit-transition: background-color 0.5s ease, -webkit-transform 0.5s ease; -ms-transition: background-color 0.5s ease, -ms-transform 0.5s ease; transition: background-color 0.5s ease, transform 0.5s ease; width: 100%; color: #ffffff; text-align: center; text-decoration: none; margin:0 auto; z-index: 3; max-width:250px; padding:15px 0; border:none; font-weight:bold; font-size:20px;}
		.custom-woocomerce-page-v .welcome-section-v a:hover{background-color:rgba(66,133,244, 0.2); color:#4285F4;}
		.custom-woocomerce-page-v{text-align:center}
		.welcome-section-v {width:100%; display:block; padding:70px;}
</style>
<div class="custom-woocomerce-page-v">
	<section class="welcome-section-v">
		<h2>Welcome to Intuition 101 2018</h2>
		<p><strong>Hello! I’m Christie Marie Sheldon! </strong><br /> With over 30,000 personal consultations over the past 15 years, I’ve learned valuable tools that are making a difference in people’s lives. My greatest joy is to help people become their abundant selves! It’s now your turn! With over 30,000 personal consultations over the past 15 years, I’ve learned valuable tools that are making a difference in people’s lives. My greatest joy is to help people become their abundant selves! <br />It’s now your turn!</p>
		<a href="https://christiesheldon.com/account/intuition-101/"> Enter > </a>
	</section> 
</div>