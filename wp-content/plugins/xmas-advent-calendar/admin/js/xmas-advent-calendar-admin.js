(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	// declare the three available templates ( if you use the video one, think about getting a lazy load plugin )
	var template1 =	'<h1 class="xmas-advent-calendar-title" style="text-align: center;">Xmas is coming!</h1>' +
					'<p class="xmas-advent-calendar-subtitle" style="text-align: center;">We wanna celebrate with you that xmas is almost here and so we are offering you this special gift. Go and download it!</p>' +
					'<img class="size-medium wp-image-8 aligncenter" src="' + template2_image['url'] + '" alt="default" />';

	var template2 = '<h1 class="xmas-advent-calendar-title" style="text-align: center;">Xmas is coming!</h1>' +
					'<p class="xmas-advent-calendar-subtitle" style="text-align: center;">We wanna celebrate with you that xmas is almost here and so we are offering you this special gift. Go and download it!</p>' +
					'<br>' +
					'<br>' +
					'<p class="xmas-advent-calendar-call-to-action-wrapper"  style="text-align: center;"><a class="xmas-advent-calendar-call-to-action" href="'+ template2_image['url'] +'" target="_blank" download="xmas_gift">Download</a></p>';

	var template3 = 'https://www.youtube.com/watch?v=ONyXSnL2LtA';
	

	// event to add templates
	jQuery( document ).on( 'click', '.xmas-advent-calendar-template-button', function() {

		addTemplate( parseInt( jQuery( this ).attr( 'data-index' ) ) );

	});

	// add template function
	function addTemplate(index) {
		
		jQuery( '#day_modal_code-html' ).click();

		switch( index ) {

			case 0:
				jQuery( '#day_modal_code' ).val( template1 );
			break;

			case 1:
				jQuery( '#day_modal_code' ).val( template2 );
			break;

			case 2:
				jQuery( '#day_modal_code' ).val( template3 );
			break;

		}
	}

})( jQuery );
