<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.yowlu.com/xmas-advent-calendar
 * @since      1.0.0
 *
 * @package    Xmas_Advent_Calendar
 * @subpackage Xmas_Advent_Calendar/admin/partials
 */
?>

<?php
	
	// aux var to know which page we want
	$requested_page = 'show';
	
	// check if edit is set
	if( isset( $_GET['action'] ) ) {

    	if( $_GET['action'] == 'edit' ) {

    		$requested_page = 'edit';
    	
    	}

    }

    // if edit
    if ( $requested_page == 'edit' ) {

    	// aux default value for the selected day
    	$day_id = 1;

    	if ( isset( $_GET['day'] ) ) {

    		if ($_GET['day'] > 0 && $_GET['day'] < 26 && is_numeric( $_GET['day'] ) ) {

    			$day_id = $_GET['day'];
    		
    		}

    	}

    	// if we are updating ( aka form data sent )
    	if($_POST['xmas_advent_calendar_day_hidden'] == 'Y') {
 			
    		// get the global wordpress database variable
			global $wpdb;

			// create a name for the table
   			$table_name = $wpdb->prefix . "xmas_advent_calendar_day";

   			// get the current position
   			// declare the query
			$sql = "SELECT position FROM " . $table_name . " WHERE id = " . $day_id . " LIMIT 1";

			// get the position
			$xmas_advent_calendar_day_positions = $wpdb->get_results($sql, ARRAY_A);
			$xmas_advent_calendar_day_position = $xmas_advent_calendar_day_positions[0]['position'];

			// if we are going to update the position
        	if( $xmas_advent_calendar_day_position != $_POST['day_position'] ) {

        		// update the other day
    			$wpdb->update( $table_name, array( 'position' => $xmas_advent_calendar_day_position, 'update_time' => current_time( 'mysql' ) ), array( 'position' => $_POST['day_position'] ) );

        	}

   			// update the day
    		$wpdb->update( $table_name, array( 'position' => $_POST['day_position'], 'image' => $_POST['day_image'], 'modal_header' => $_POST['day_header'], 'modal_code' => stripslashes( wp_filter_post_kses( addslashes( $_POST['day_modal_code'] ) ) ), 'update_time' => current_time( 'mysql' ) ), array( 'id' => $day_id ) );
        	
?>

<div class="updated">
	<p>
		<strong>
			<?php _e( 'Options saved.' ); ?>
		</strong>
	</p>
</div>

<?php

    }

    // normal page display
    // get the global wordpress database variable
	global $wpdb;

	// create a name for the table
   	$table_name = $wpdb->prefix . "xmas_advent_calendar_day";
				
   	// declare the query
	$sql = "SELECT * FROM " . $table_name . " WHERE id = " . $day_id . " LIMIT 1";

	// get the days
	$xmas_advent_calendar_day = $wpdb->get_results($sql, ARRAY_A);

?>

<div class="wrap">
	<?php    echo "<h1>" . __( 'Xmas Advent Calendar Day' ) . ' ' . $day_id . ' ' . __( 'Settings' ) . "</h1>"; ?>

	<form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<input type="hidden" name="xmas_advent_calendar_day_hidden" value="Y">
		
		<?php
			
			echo "<h3>" . __( 'Calendar Settings' ) . "</h3>";

		?>

		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">
						<?php    echo __( 'Position' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Position' ) ; ?>
								</span>
							</legend>
							<label for="day_position">
								<select name="day_position" id="day_position">
									<?php 

										for( $i = 1; $i < 26; $i++ ) {

											// output the position
											echo '<option value="' . $i . '" ' . ( ( $i == $xmas_advent_calendar_day[0]['position'] ) ? 'selected="selected"' : '' ) . '>' . $i . '</option>';

										}

									?>
								</select>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Image' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Image' ) ; ?>
								</span>
							</legend>
							<?php 

								for( $i = 1; $i < 26; $i++ ) {

									// output the images
									echo '<label class="rad" for="day_image">';
									echo '<input name="day_image" type="radio" id="day_image' . $i . '" value="public/images/xmas-advent-calendar-image-' . $i . '.png" ' . ( ( $xmas_advent_calendar_day[0]['image'] == "public/images/xmas-advent-calendar-image-" . $i . ".png" ) ? 'checked="checked"' : '' ) . '>';
									echo '<div class="admin-xmas-advent-calendar-img-wrapper"><img class="admin-xmas-advent-calendar-img" alt="image ' . $i . '" title="image ' . $i . '" src="' . plugins_url() . '/xmas-advent-calendar/public/images/xmas-advent-calendar-image-' . $i . '.png"></div>';
									echo '</label>';
								}

							?>
						</fieldset>
					</td>
				</tr>
			</tbody>
		</table>
		
		<?php
			
			echo "<h3>" . __( 'Modal Settings' ) . "</h3>";
		
		?>

		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">
						<?php    echo __( 'Header' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Header' ) ; ?>
								</span>
							</legend>
							<?php 

								for( $i = 1; $i < 9; $i++ ) {

									// output the headers
									echo '<label class="rad" for="day_header">';
									echo '<input name="day_header" type="radio" class="header-input" id="day_header' . $i . '" value="public/images/xmas-advent-calendar-header-' . $i . '.png" ' . ( ( $xmas_advent_calendar_day[0]['modal_header'] == "public/images/xmas-advent-calendar-header-" . $i . ".png" ) ? 'checked="checked"' : '' ) . '>';
									echo '<div class="admin-xmas-advent-calendar-img-wrapper header-input-wrapper"><img class="admin-xmas-advent-calendar-img" alt="header ' . $i . '" title="header ' . $i . '" src="' . plugins_url() . '/xmas-advent-calendar/public/images/xmas-advent-calendar-header-' . $i . '.png"></div>';
									echo '</label>';
								}

							?>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Modal Code' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Modal Code' ) ; ?>
								</span>
							</legend>
							<?php 

								// output a WYSIWYG editor
								wp_editor( $xmas_advent_calendar_day[0]['modal_code'], $id = 'day_modal_code', array( 'media_buttons' => true, 'tinymce' => true, 'quicktags' => true ) );

							?>
						</fieldset>
						<br>
						<h4><?php 	echo __( 'Available Templates' ) ; ?></h4>
						<button class="button button-default xmas-advent-calendar-template-button" data-index="0" type="button"><?php 	echo __( 'Text & Image' ) ; ?></button>
						<button class="button button-default xmas-advent-calendar-template-button" data-index="1" type="button"><?php 	echo __( 'Downloadable Element' ) ; ?></button>
						<button class="button button-default xmas-advent-calendar-template-button" data-index="2" type="button"><?php 	echo __( 'Video' ) ; ?></button>
					</td>
				</tr>
			</tbody>
		</table>
		
		<p class="submit">
			<?php    echo '<input type="submit" name="submit" id="submit" class="button button-primary" value="' . __( 'Save changes' ) . '">'; ?>
		</p>
	</form>
		
</div>

<?php

    }
    else {
    	// if main page

    	// if we are updating ( aka form data sent )
        if(isset($_POST['xmas_advent_calendar_hidden']) && $_POST['xmas_advent_calendar_hidden'] == 'Y') {

			// get data
        	$xmas_advent_calendar_allow_past_days = (!isset($_POST['xmas_advent_calendar_allow_past_days'])) ? false : $_POST['xmas_advent_calendar_allow_past_days'];
        	$xmas_advent_calendar_toggle = $_POST['xmas_advent_calendar_toggle'];
        	$xmas_advent_calendar_color = $_POST['xmas_advent_calendar_color'];
        	$xmas_advent_calendar_open_past_days = (!isset($_POST['xmas_advent_calendar_open_past_days'])) ? false : $_POST['xmas_advent_calendar_open_past_days'];
        	$xmas_advent_calendar_type = $_POST['xmas_advent_calendar_type'];
        	$xmas_advent_calendar_start_date = $_POST['xmas_advent_calendar_start_date'];
        	$xmas_advent_calendar_end_date = $_POST['xmas_advent_calendar_end_date'];
        	$xmas_advent_calendar_show_calendar_now = (!isset($_POST['xmas_advent_calendar_show_calendar_now'])) ? false : $_POST['xmas_advent_calendar_show_calendar_now'];
        	$xmas_advent_calendar_simulate = (!isset($_POST['xmas_advent_calendar_simulate'])) ? false : $_POST['xmas_advent_calendar_simulate'];
        	$xmas_advent_calendar_simulate_date = $_POST['xmas_advent_calendar_simulate_date'];
        	$xmas_advent_calendar_timezone = $_POST['xmas_advent_calendar_timezone'];
        	$xmas_advent_calendar_24_days = (!isset($_POST['xmas_advent_calendar_24_days'])) ? false : $_POST['xmas_advent_calendar_24_days'];

        	// update data
        	update_option('xmas_advent_calendar_allow_past_days', $xmas_advent_calendar_allow_past_days);
        	update_option('xmas_advent_calendar_toggle', $xmas_advent_calendar_toggle);
        	update_option('xmas_advent_calendar_color', $xmas_advent_calendar_color);
        	update_option('xmas_advent_calendar_open_past_days', $xmas_advent_calendar_open_past_days);
        	update_option('xmas_advent_calendar_type', $xmas_advent_calendar_type);
        	if ( Xmas_Advent_Calendar_Loader::validate_date( $xmas_advent_calendar_start_date ) ) {

        		update_option('xmas_advent_calendar_start_date', $xmas_advent_calendar_start_date);

        	}
        	if ( Xmas_Advent_Calendar_Loader::validate_date( $xmas_advent_calendar_end_date ) ) {

        		update_option('xmas_advent_calendar_end_date', $xmas_advent_calendar_end_date);

        	}
        	update_option('xmas_advent_calendar_show_calendar_now', $xmas_advent_calendar_show_calendar_now);
        	update_option('xmas_advent_calendar_simulate', $xmas_advent_calendar_simulate);
        	if ( Xmas_Advent_Calendar_Loader::validate_date( $xmas_advent_calendar_simulate_date ) ) {

        		update_option('xmas_advent_calendar_simulate_date', $xmas_advent_calendar_simulate_date);

        	}
        	update_option('xmas_advent_calendar_timezone', $xmas_advent_calendar_timezone);
        	update_option('xmas_advent_calendar_24_days', $xmas_advent_calendar_24_days);

?>

<div class="updated">
	<p>
		<strong>
			<?php _e('Options saved.' ); ?>
		</strong>
	</p>
</div>

<?php

    } else {

        // normal page display
        $xmas_advent_calendar_allow_past_days = get_option('xmas_advent_calendar_allow_past_days');
        $xmas_advent_calendar_toggle = get_option('xmas_advent_calendar_toggle');
        $xmas_advent_calendar_color = get_option('xmas_advent_calendar_color');
        $xmas_advent_calendar_open_past_days = get_option('xmas_advent_calendar_open_past_days');
        $xmas_advent_calendar_type = get_option('xmas_advent_calendar_type', 'side_sticky');
        $xmas_advent_calendar_start_date = get_option('xmas_advent_calendar_start_date', '2017-12-01');
        $xmas_advent_calendar_end_date = get_option('xmas_advent_calendar_end_date', '2017-12-25' );
        $xmas_advent_calendar_show_calendar_now = get_option('xmas_advent_calendar_show_calendar_now', true);
        $xmas_advent_calendar_simulate = get_option('xmas_advent_calendar_simulate', false);
        $xmas_advent_calendar_simulate_date = get_option('xmas_advent_calendar_simulate_date', '2017-12-25');
        $xmas_advent_calendar_timezone = get_option('xmas_advent_calendar_timezone', '');
        $xmas_advent_calendar_24_days = get_option('xmas_advent_calendar_24_days', false);

    }

?>

<div class="wrap">
	<?php    echo "<h1>" . __( 'Xmas Advent Calendar Settings' ) . "</h1>"; ?>

	<form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<input type="hidden" name="xmas_advent_calendar_hidden" value="Y">
		<h3 class="title">
			<?php    echo __( 'Global Settings' ); ?>
		</h3>
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">
						<?php    echo __( 'Start date' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Start date' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_start_date">
								<?php 	 echo '<input name="xmas_advent_calendar_start_date" class="datepicker" type="date" id="xmas_advent_calendar_start_date" value="' . $xmas_advent_calendar_start_date . '" >' ; ?>
								<?php    echo __( 'Set the first day the calendar should start. (Follow the default format you see)' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'End date' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'End date' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_end_date">
								<?php 	 echo '<input name="xmas_advent_calendar_end_date" type="date" id="xmas_advent_calendar_end_date" value="' . $xmas_advent_calendar_end_date . '" >' ; ?>
								<?php    echo __( 'Set the day the calendar should end. (Follow the default format you see)' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Set timezone' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Set timezone' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_timezone">
								<select name="xmas_advent_calendar_timezone" id="xmas_advent_calendar_timezone">
									<?php
										$zones = timezone_identifiers_list();
										array_unshift($zones, '');
										foreach ($zones as $t) {
											echo '<option value="' . $t . '" '. (($xmas_advent_calendar_timezone == $t) ? 'selected' : '') .'>'.$t.'</option>';
								  		}
								  	?>
								</select>
								<?php    echo __( 'Set the timezone you would like to use. (Let it empty by default. Only change it if the calendar is not working with you current time)' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Show calendar' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Show calendar' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_show_calendar_now">
								<!-- output the user can open past days option -->
								<?php 	 echo '<input name="xmas_advent_calendar_show_calendar_now" type="checkbox" id="xmas_advent_calendar_show_calendar_now" value="true" ' . ( ( $xmas_advent_calendar_show_calendar_now == "true" ) ? 'checked="checked"' : '' ) . '>' ; ?>
								<?php    echo __( 'Show calendar now. It will be shown but it will start working the date you set on the "Start date" field' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<hr>
					</th>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Simulate' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Simulate' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_simulate">
								<?php 	 echo '<input name="xmas_advent_calendar_simulate" type="checkbox" id="xmas_advent_calendar_simulate" value="true" ' . ( ( $xmas_advent_calendar_simulate == "true" ) ? 'checked="checked"' : '' ) . '>' ; ?>
								<?php    echo __( 'If you would like to see how it will work on a specific date, check this option and set a date below on "Simulate date"' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Simulate date' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Simulate date' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_simulate_date">
								<?php 	 echo '<input name="xmas_advent_calendar_simulate_date" class="datepicker" type="date" id="xmas_advent_calendar_simulate_date" value="' . $xmas_advent_calendar_simulate_date . '" >' ; ?>
								<?php    echo __( 'The calendar will work as id today was the date you set here. (Follow the default format you see)' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<hr>
					</th>
					<td>
						<hr>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Type of display' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Type of Display' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_type">
								<input name="xmas_advent_calendar_type" type="radio" id="type1" value="side_sticky" <?php echo ( ( $xmas_advent_calendar_type == "side_sticky" ) ? 'checked="checked"' : '' ); ?> >
								<?php    echo __( 'The calendar is shown as a side sticky element on every page side without using a shortcode' ) ; ?>
							</label>
							<br>
							<label for="xmas_advent_calendar_type">
								<input name="xmas_advent_calendar_type" type="radio" id="type2" value="sc_side_sticky" <?php echo ( ( $xmas_advent_calendar_type == "sc_side_sticky" ) ? 'checked="checked"' : '' ); ?> >
								<?php    echo __( 'The calendar is shown as a side sticky element when a shortcode is added' ) ; ?>
							</label>
							<br>
							<label for="xmas_advent_calendar_type">
								<input name="xmas_advent_calendar_type" type="radio" id="type3" value="sc_big" <?php echo ( ( $xmas_advent_calendar_type == "sc_big" ) ? 'checked="checked"' : '' ); ?> >
								<?php    echo __( 'The calendar is shown big when a shortcode is added' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Show only 24 days' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Show only 24 days' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_24_days">
								<?php 	 echo '<input name="xmas_advent_calendar_24_days" type="checkbox" id="xmas_advent_calendar_24_days" value="true" ' . ( ( $xmas_advent_calendar_24_days == "true" ) ? 'checked="checked"' : '' ) . '>' ; ?>
								<?php    echo __( 'Calendar will display 24 days (european style) instead of the default 25 (usa style)' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Allow past days' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Allow past days' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_allow_past_days">
								<?php 	 echo '<input name="xmas_advent_calendar_allow_past_days" type="checkbox" id="xmas_advent_calendar_allow_past_days" value="true" ' . ( ( $xmas_advent_calendar_allow_past_days == "true" ) ? 'checked="checked"' : '' ) . '>' ; ?>
								<?php    echo __( 'Users can open past days' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Past days opened' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Past days opened' ) ; ?>
								</span>
							</legend>
							<label for="xmas_advent_calendar_past_days_opened">
								<!-- output the past days are opened option -->
								<?php 	 echo '<input name="xmas_advent_calendar_open_past_days" type="checkbox" id="xmas_advent_calendar_open_past_days" value="true" ' . ( ( $xmas_advent_calendar_open_past_days == "true" ) ? 'checked="checked"' : '' ) . '>' ; ?>
								<?php    echo __( 'Past days are opened' ) ; ?>
							</label>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Choose the toggle' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Choose the toggle' ) ; ?>
								</span>
							</legend>
							<?php 

								for( $i = 1; $i < 11; $i++ ) {

									// output the toggles
									echo '<label class="rad" for="xmas_advent_calendar_toggle">';
									echo '<input name="xmas_advent_calendar_toggle" type="radio" class="toggle-input" id="xmas_advent_calendar_toggle"' . $i . '" value="public/images/icon-advent-calendar-' . $i . '.png" ' . ( ( $xmas_advent_calendar_toggle == "public/images/icon-advent-calendar-" . $i . ".png" ) ? 'checked="checked"' : '' ) . '>';
									echo '<div class="admin-xmas-advent-calendar-img-wrapper toggle-input-wrapper"><img class="admin-xmas-advent-calendar-img" alt="toggle ' . $i . '" title="toggle" ' . $i . '" src="' . plugins_url() . '/xmas-advent-calendar/public/images/icon-advent-calendar-' . $i . '.png"></div>';
									echo '</label>';

								}

							?>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<?php    echo __( 'Choose the background color' ) ; ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span>
									<?php    echo __( 'Choose the background color' ) ; ?>
								</span>
							</legend>
							<?php 

								for( $i = 1; $i < 7; $i++ ) {

									// output the colors
									echo '<label class="rad" for="xmas_advent_calendar_color">';
									echo '<input name="xmas_advent_calendar_color" type="radio" class="color-input" id="xmas_advent_calendar_color"' . $i . '" value="color-input-wrapper-' . $i . '" ' . ( ( $xmas_advent_calendar_color == "color-input-wrapper-" . $i ) ? 'checked="checked"' : '' ) . '>';
									echo '<div class="admin-xmas-advent-calendar-img-wrapper color-input-wrapper color-input-wrapper-' . $i . '"></div>';
									echo '</label>';
									
								}

							?>
						</fieldset>
					</td>
				</tr>
			</tbody>
		</table>
		<p class="submit">
			<?php    echo '<input type="submit" name="submit" id="submit" class="button button-primary" value="' . __( 'Save changes' ) . '">'; ?>
		</p>
	</form>
		
	<h3 class="title">
		<?php    echo __( 'Days Settings' ); ?>
	</h3>
	<p>
		<?php    echo __( 'You can modify for each day which image do you want and its modal code along other settings.' ); ?>
	</p>

	<?php

		// get the global wordpress database variable
		global $wpdb;

		// create a name for the table
   		$table_name = $wpdb->prefix . "xmas_advent_calendar_day";
				
   		// declare the query
		$sql = "SELECT * FROM " . $table_name;

		// get the days
		$xmas_advent_calendar_days = $wpdb->get_results($sql, ARRAY_A);

	?>

	<div class="tablenav top">
		<div class="tablenav-pages one-page">
			<span class="displaying-num"><?php echo count( $xmas_advent_calendar_days ) . ' ' . __( 'elements' ); ?></span>
		</div>		
		<br class="clear">
	</div>
	
	<table class="wp-list-table widefat fixed striped posts">
		<thead>
			<tr>
				<td id="cb" class="manage-column column-cb check-column">
					<label class="screen-reader-text" for="cb-select-all-1">Seleccionar todos</label>
					<input id="cb-select-all-1" type="checkbox">
				</td>
				<th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
					<a href="http://www.yowlu.com/wordpress/wp-admin/edit.php?orderby=title&amp;order=asc"><span>Day</span><span class="sorting-indicator"></span></a>
				</th>
				<th scope="col" id="image" class="manage-column column-image">Image</th>
				<th scope="col" id="author" class="manage-column column-author">Position</th>
				<th scope="col" id="date" class="manage-column column-date sortable asc"><a href="http://www.yowlu.com/wordpress/wp-admin/edit.php?orderby=date&amp;order=desc"><span>Date</span><span class="sorting-indicator"></span></a></th>
			</tr>
		</thead>

		<tbody id="the-list">
			
			<?php 
				
				// loop for all the days
				foreach( $xmas_advent_calendar_days as $row ) {

			?>
			
			<tr id="xmas-advent-calendar-day-<?php echo $row['id'] ;?>" class="iedit author-self level-0 xmas-advent-calendar-day-<?php echo $row['id'];?> type-post status-publish format-standard hentry xfolkentry">
				<th scope="row" class="check-column">			
					<label class="screen-reader-text" for="cb-select-<?php echo $row['id'] ;?>"> <?php echo __( 'Choose' ) . ' ' . __( 'day' ) . ' ' . $row['id']; ?></label>
					<input id="cb-select-<?php echo $row['id'] ;?>" type="checkbox" name="xmas_advent_calendar_day[]" value="<?php echo $row['id'] ;?>">
					<div class="locked-indicator"></div>
				</th>

				<td class="title column-title has-row-actions column-primary page-title" data-colname="Day">
					<strong>
						<a class="row-title" href="<?php echo strtok($_SERVER["REQUEST_URI"],'?') . '?page=xmas-advent-calendar&action=edit&day=' . $row['id']; ?>" title="<?php echo __( 'Edit' ) . ' ' . __( 'day' ) . ' ' . $row['id'];?> "><?php echo __( 'Day' ) . ' ' . $row['id'];?></a>
					</strong>
					<div class="locked-info">
						<span class="locked-avatar"></span> 
						<span class="locked-text"></span>
					</div>

					<div class="row-actions">
						<span class="edit">
							<a href="<?php echo strtok($_SERVER["REQUEST_URI"],'?') . '?page=xmas-advent-calendar&action=edit&day=' . $row['id']; ?>" title="<?php echo __( 'Edit this element' ); ?>"><?php echo __( 'Edit' ); ?></a> 
						</span>
					</div>
				</td>
				
				<td class="image column-image" data-colname="Image">
					<img class="admin-xmas-advent-calendar-img" alt="day <?php echo $row['id'] ;?> image" title="day <?php echo $row['id'] ;?> image" src="<?php echo plugins_url() . '/xmas-advent-calendar/' . $row['image'] ;?>">
				</td>
				
				<td class="author column-author" data-colname="Position">
					<?php echo $row['position'] ;?>
				</td>

				<td class="date column-date" data-colname="Date">
					<abbr title="<?php echo $row['update_time'] ;?>"><?php echo $row['update_time'] ;?></abbr>
				</td>
			<?php
				}
			?>

		</tbody>

		<tfoot>
			<tr>
				<td class="manage-column column-cb check-column">
					<label class="screen-reader-text" for="cb-select-all-2">Seleccionar todos</label>
					<input id="cb-select-all-2" type="checkbox">
				</td>
				<th scope="col" class="manage-column column-title column-primary sortable desc">
					<a href="http://www.yowlu.com/wordpress/wp-admin/edit.php?orderby=title&amp;order=asc"><span>Day</span><span class="sorting-indicator"></span></a>
				</th>
				<th scope="col" class="manage-column column-image">Image</th>
				<th scope="col" class="manage-column column-author">Position</th>
				<th scope="col" class="manage-column column-date sortable asc">
					<a href="http://www.yowlu.com/wordpress/wp-admin/edit.php?orderby=date&amp;order=desc"><span>Date</span><span class="sorting-indicator"></span></a>
				</th>	
			</tr>
		</tfoot>
	</table>

	<div class="tablenav bottom">
		<div class="tablenav-pages one-page"><span class="displaying-num"><?php echo count( $xmas_advent_calendar_days ) . ' ' . __( 'elements' ); ?></span>
		<br class="clear">
	</div>
		
</div>

<?php
    }
?>