<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.yowlu.com/xmas-advent-calendar
 * @since      1.0.0
 *
 * @package    Xmas_Advent_Calendar
 * @subpackage Xmas_Advent_Calendar/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Xmas_Advent_Calendar
 * @subpackage Xmas_Advent_Calendar/admin
 * @author     Your Name <info@yowlu.com>
 */
class Xmas_Advent_Calendar_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/xmas-advent-calendar-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/xmas-advent-calendar-admin.js', array( 'jquery' ), $this->version, false );

		wp_localize_script( $this->plugin_name, 'template2_image', array( 'url' =>  plugins_url() . '/xmas-advent-calendar/public/images/photo-default.png' ) );

		// enqueue scripts needed to work with WYSIWYG in-built editor
		wp_enqueue_script('post');
		
		// check if he can edit (just for security)
		if ( user_can_richedit() ) {
  			
  			wp_enqueue_script('editor');
  		
  		}
		
		// add a thickbox
		add_thickbox();
		
		// enqueue the media upload
		wp_enqueue_script('media-upload');
		
		// enqueue the word count
		wp_enqueue_script('word-count');

	}

}
