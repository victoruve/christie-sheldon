<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.yowlu.com/xmas-advent-calendar
 * @since             1.0.0
 * @package           Xmas_Advent_Calendar
 *
 * @wordpress-plugin
 * Plugin Name:       Xmas Advent Calendar
 * Plugin URI:        http://www.yowlu.com/xmas-advent-calendar
 * Description:       A customizable advent calendar.
 * Version:           1.0.0
 * Author:            Yowlu
 * Author URI:        http://www.yowlu.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       xmas-advent-calendar
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-xmas-advent-calendar-activator.php
 */
function activate_xmas_advent_calendar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-xmas-advent-calendar-activator.php';
	Xmas_Advent_Calendar_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-xmas-advent-calendar-deactivator.php
 */
function deactivate_xmas_advent_calendar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-xmas-advent-calendar-deactivator.php';
	Xmas_Advent_Calendar_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_xmas_advent_calendar' );
register_deactivation_hook( __FILE__, 'deactivate_xmas_advent_calendar' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-xmas-advent-calendar.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_xmas_advent_calendar() {

	$plugin = new Xmas_Advent_Calendar();
	$plugin->run();

}
run_xmas_advent_calendar();
