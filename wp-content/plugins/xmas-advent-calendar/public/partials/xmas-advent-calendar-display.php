<?php

if ( get_option( 'xmas_advent_calendar_timezone', '' ) != '' ) {
	date_default_timezone_set( get_option( 'xmas_advent_calendar_timezone', '' ) );
}

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.yowlu.com/xmas-advent-calendar
 * @since      1.0.0
 *
 * @package    Xmas_Advent_Calendar
 * @subpackage Xmas_Advent_Calendar/public/partials
 */
?>

<!-- Calendar --> 
<?php 	
	// set the classes to display the different types
	$wrapper_class = ( ( get_option( 'xmas_advent_calendar_type', 'side_sticky' ) == 'side_sticky' || get_option( 'xmas_advent_calendar_type', 'side_sticky' ) == 'sc_side_sticky' ) ? 'xmas-advent-calendar__wrapper--side-sticky' : 'xmas-advent-calendar__wrapper--big' ) ;
	
	echo '<div class="xmas-advent-calendar__wrapper ' . $wrapper_class . ' ' . get_option( 'xmas_advent_calendar_color' ) . '">'; ?>
	
	<div class="xmas-advent-calendar__wrapper-toggle">
		<!-- set the toggle from what it is stored -->
		<?php 	echo '<img class="xmas-advent-calendar__day" src="' . plugins_url() . '/xmas-advent-calendar/' . get_option( 'xmas_advent_calendar_toggle' ) . '">'; ?>
	</div>

	<?php
		
		// get the global wordpress database variable
		global $wpdb;

		// create a name for the table
   		$table_name = $wpdb->prefix . "xmas_advent_calendar_day";
				
   		// declare the query
		$sql = "SELECT * FROM " . $table_name . " ORDER BY position ASC";

		// get the days
		$xmas_advent_calendar_days = $wpdb->get_results($sql, ARRAY_A);

		// get the past days option
		$xmas_advent_calendar_allow_past_days = get_option('xmas_advent_calendar_allow_past_days');

		// get the open days option
		$xmas_advent_calendar_open_past_days = get_option( 'xmas_advent_calendar_open_past_days' );

		// get the simulate option
		$xmas_advent_calendar_simulate = get_option('xmas_advent_calendar_simulate', true);

		// get the simulate date option
		$xmas_advent_calendar_simulate_date = new DateTime( get_option('xmas_advent_calendar_simulate_date', '2017-12-25') );

		// get the start date option
		$xmas_advent_calendar_start_date = new DateTime( get_option('xmas_advent_calendar_start_date', '2017-12-01') );

		// get the end date option
		$xmas_advent_calendar_end_date = new DateTime( get_option('xmas_advent_calendar_end_date', '2017-12-25') );

		// loop for all the days
		foreach( $xmas_advent_calendar_days as $row ) {

			// get an image id
			$aux_image_id = explode('-', $row['image']);
			$aux_image_id2 = $aux_image_id[count($aux_image_id) - 1];
			$aux_image_id3 = explode('.', $aux_image_id2);
			$image_id = $aux_image_id3[0];

			// check $wrapper_allowed
			$wrapper_allowed = false;
			$today = new DateTime();

			if ( $xmas_advent_calendar_simulate ) {
				$today = $xmas_advent_calendar_simulate_date;
			}

			if ( $today >= $xmas_advent_calendar_end_date && $xmas_advent_calendar_allow_past_days == "true" ) {
				$wrapper_allowed = true;
			}
			else if ( $today >= $xmas_advent_calendar_start_date ) {
				if ( $today->format( 'd' ) >= $row['id'] && $xmas_advent_calendar_allow_past_days == "true" ) {
					$wrapper_allowed = true;
				}
				else if ( $today->format( 'd' ) == $row['id'] ) {
					$wrapper_allowed = true;
				}
			}

			// check $wrapper_opened
			$wrapper_opened = false;

			if ( $today >= $xmas_advent_calendar_end_date && $xmas_advent_calendar_open_past_days == "true" ) {
				$wrapper_opened = true;
			}
			else if ( $today >= $xmas_advent_calendar_start_date && $xmas_advent_calendar_open_past_days == "true" && $today->format( 'd' ) >= $row['id'] ) {
				$wrapper_opened = true;
			}

			// output it
			echo '<div data-id="' . $row['id'] . '" class="xmas-advent-calendar__day-wrapper ' . ( $wrapper_allowed ? 'xmas-advent-calendar__day-wrapper--allowed' : '' )  . '">' .
					'<div class="xmas-advent-calendar__day-background ' . ( $wrapper_opened ? 'xmas-advent-calendar__day-wrapper--opened' : '' ) . '">' .
						'<img class="xmas-advent-calendar__day" src="' . plugins_url() . '/xmas-advent-calendar/' . $row['image'] . '">' .
						'<span id="xmas-advent-calendar__day-text' . $image_id . '" class="xmas-advent-calendar__day-text">' . $row['id'] . '</span>' .
					'</div>' .
				'</div>';

		}

	?>

	<?php
		if ( get_option ( 'xmas_advent_calendar_24_days' ) ) {
	?>
		<style>
			.xmas-advent-calendar__day-wrapper[data-id="25"] {
	    		display: none;
			}
		</style>
	<?php
		}
	?>

</div>

<!-- Modal -->
<div class="xmas-advent-calendar__modal">
	<div class="xmas-advent-calendar__modal__content">
		<!-- snow animation -->
		<div id="snow">
		</div>
		<!-- close button -->
		<div class="close-button">
			<img alt="modal close image" title="modal close image" src="<?php echo plugins_url() . '/xmas-advent-calendar/public/images/close-icon.png';?>">
		</div>

		<?php

			foreach( $xmas_advent_calendar_days as $row ) {

				// check $wrapper_allowed
				$wrapper_allowed = false;
				$today = new DateTime();

				if ( $xmas_advent_calendar_simulate ) {
					$today = $xmas_advent_calendar_simulate_date;
				}

				if ( $today >= $xmas_advent_calendar_end_date && $xmas_advent_calendar_allow_past_days == "true" ) {
					$wrapper_allowed = true;
				}
				else if ( $today >= $xmas_advent_calendar_start_date ) {
					if ( $today->format( 'd' ) >= $row['id'] && $xmas_advent_calendar_allow_past_days == "true" ) {
						$wrapper_allowed = true;
					}
					else if ( $today->format( 'd' ) == $row['id'] ) {
						$wrapper_allowed = true;
					}
				}

				if ( $wrapper_allowed ) {

					// get the header id
					$aux_header_id = explode('-', $row['modal_header']);
					$aux_header_id2 = $aux_header_id[count($aux_header_id) - 1];
					$aux_header_id3 = explode('.', $aux_header_id2);
					$header_id = $aux_header_id3[0];

					// echo the header
					echo '<div class="xmas-advent-calendar__modal-header xmas-advent-calendar__modal-header--' . $header_id . '" id="xmas-advent-calendar__modal-header-' . $row['id'] . '"><img alt="modal header" title="modal header" src="' . plugins_url() . '/xmas-advent-calendar/' . $row['modal_header'] . '"></div>';
					
					// echo the modal
					echo '<div class="xmas-advent-calendar__modal-code" id="xmas-advent-calendar__modal-code-' . $row['id'] . '">' . apply_filters( 'the_content', $row['modal_code'] ) . '</div>';

				}

			}

		?>

	</div>
</div>