=== Xmas Advent Calendar ===
Author: Yowlu
Tags: xmas, advent, calendar
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: All rights reserved

This customizable xmas advent calendar provides you a nice and attractive calendar which allows the user to open a new gift you decide each day.

== Description ==

This customizable xmas advent calendar provides you a nice and attractive calendar which allows the user to open a new gift you decide each day.

This is a perfect way to engage the users and make them return to your website every day until Christmas.


== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `xmas-advent-calendar` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress