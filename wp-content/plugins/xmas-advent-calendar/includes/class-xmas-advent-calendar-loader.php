<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       http://www.yowlu.com/xmas-advent-calendar
 * @since      1.0.0
 *
 * @package    Xmas_Advent_Calendar
 * @subpackage Xmas_Advent_Calendar/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Xmas_Advent_Calendar
 * @subpackage Xmas_Advent_Calendar/includes
 * @author     Yowlu <info@yowlu.com>
 */
class Xmas_Advent_Calendar_Loader {

	/**
	 * The array of actions registered with WordPress.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array    $actions    The actions registered with WordPress to fire when the plugin loads.
	 */
	protected $actions;

	/**
	 * The array of filters registered with WordPress.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array    $filters    The filters registered with WordPress to fire when the plugin loads.
	 */
	protected $filters;

	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->actions = array();
		$this->filters = array();

	}

	/**
	 * Add a new action to the collection to be registered with WordPress.
	 *
	 * @since    1.0.0
	 * @param    string               $hook             The name of the WordPress action that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the action is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         Optional. he priority at which the function should be fired. Default is 10.
	 * @param    int                  $accepted_args    Optional. The number of arguments that should be passed to the $callback. Default is 1.
	 */
	public function add_action( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
		$this->actions = $this->add( $this->actions, $hook, $component, $callback, $priority, $accepted_args );
	}

	/**
	 * Add a new filter to the collection to be registered with WordPress.
	 *
	 * @since    1.0.0
	 * @param    string               $hook             The name of the WordPress filter that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the filter is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         Optional. he priority at which the function should be fired. Default is 10.
	 * @param    int                  $accepted_args    Optional. The number of arguments that should be passed to the $callback. Default is 1
	 */
	public function add_filter( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
		$this->filters = $this->add( $this->filters, $hook, $component, $callback, $priority, $accepted_args );
	}

	/**
	 * A utility function that is used to register the actions and hooks into a single
	 * collection.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @param    array                $hooks            The collection of hooks that is being registered (that is, actions or filters).
	 * @param    string               $hook             The name of the WordPress filter that is being registered.
	 * @param    object               $component        A reference to the instance of the object on which the filter is defined.
	 * @param    string               $callback         The name of the function definition on the $component.
	 * @param    int                  $priority         The priority at which the function should be fired.
	 * @param    int                  $accepted_args    The number of arguments that should be passed to the $callback.
	 * @return   array                                  The collection of actions and filters registered with WordPress.
	 */
	private function add( $hooks, $hook, $component, $callback, $priority, $accepted_args ) {

		$hooks[] = array(
			'hook'          => $hook,
			'component'     => $component,
			'callback'      => $callback,
			'priority'      => $priority,
			'accepted_args' => $accepted_args
		);

		return $hooks;

	}

	/**
	 * Register the filters and actions with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {

		foreach ( $this->filters as $hook ) {
			add_filter( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
		}

		foreach ( $this->actions as $hook ) {
			add_action( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
		}

		// create the admin section
		$this->create_admin_action();

		// create the calendar
		$this->create_calendar_action();

		// add a filter
		$this->set_mce_link_download_attribute();

		// add a shortcode
		$this->create_calendar_shortcode();

	}

	/**
	 * Create an action inside the settings menu
	 *
	 * @since    1.0.0
	 */
	public function create_admin_action() {

		add_action( 'admin_menu', array( $this, 'add_xmas_option' ) );

	}

	/**
	 * Add an options page for the plugin
	 *
	 * @since    1.0.0
	 */
	public function add_xmas_option() {

		add_menu_page( 'Xmas Advent Calendar', 'Xmas Advent Calendar', 'edit_pages', 'xmas-advent-calendar', array( $this, 'include_admin_page' ) );
	}

	/**
	 * Include the partial file
	 *
	 * @since    1.0.0
	 */
	public function include_admin_page() {

		include( plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/xmas-advent-calendar-admin-display.php' );

	}

	/**
	 * Create and action inside the footer
	 *
	 * @since    1.0.0
	 */
	public function create_calendar_action() {

		add_action( 'wp_footer', array( $this, 'show_xmas_advent_calendar' ) );

	}

	/**
	 * Include the partial file
	 *
	 * @since    1.0.0
	 */
	public function show_xmas_advent_calendar() {

		if ( get_option( 'xmas_advent_calendar_timezone', '' ) != '' ) {
			date_default_timezone_set( get_option( 'xmas_advent_calendar_timezone', '' ) );
		}

		if ( get_option( 'xmas_advent_calendar_type', 'side_sticky' ) == 'side_sticky' && get_option( 'xmas_advent_calendar_show_calendar_now', true ) ) {
			
			include( plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/xmas-advent-calendar-display.php' );
		
		}
		
	}

	/**
	 * Create a filter for the wp_editor download attribute
	 *
	 * @since    1.0.0
	 */
	public function set_mce_link_download_attribute() {

		add_action( 'init', array( $this, 'download_attribute' ) );

	}

	/**
	 * Add the filter
	 *
	 * @since    1.0.0
	 */
	public function download_attribute() {
		global $allowedposttags;

		$default = empty ( $allowedposttags['a'] ) ? array () : $allowedposttags[ 'a' ];
		$custom  = array (
		    'download' => true
		);

		$allowedposttags[ 'a' ] = array_merge( $default, $custom );
	}

	/**
	 * Create a shortcode for the calendar
	 *
	 * @since    1.0.0
	 */
	public function create_calendar_shortcode() {

		if ( get_option( 'xmas_advent_calendar_timezone', '' ) != '' ) {
			date_default_timezone_set( get_option( 'xmas_advent_calendar_timezone', '' ) );
		}

		if ( get_option( 'xmas_advent_calendar_type', 'side_sticky' ) != 'side_sticky' && get_option( 'xmas_advent_calendar_show_calendar_now', true ) ) {
		
			add_shortcode( 'xmas-advent-calendar', array( $this, 'xmas_advent_calendar_shortcode' ) );

		}

	}

	/**
	 * Handler of the shortcode
	 *
	 * @since    1.0.0
	 */
	public function xmas_advent_calendar_shortcode() {

		ob_start();
		include( plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/xmas-advent-calendar-display.php' );
    	return ob_get_clean(); 

	}

	/**
	 * Aux function used to validate dates
	 *
	 * @since    1.0.0
	 */
	 function validate_date( $date ) {

    	$d = DateTime::createFromFormat( 'Y-m-d', $date );
    	return $d && $d->format( 'Y-m-d' ) == $date;

	}

}
