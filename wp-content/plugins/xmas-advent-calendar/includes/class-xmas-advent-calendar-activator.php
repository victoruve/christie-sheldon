<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.yowlu.com/xmas-advent-calendar
 * @since      1.0.0
 *
 * @package    Xmas_Advent_Calendar
 * @subpackage Xmas_Advent_Calendar/includes
 */

/**
 * Curent db version.
 *
 * Current db version. It will be used for further updates.
 *
 * @since    1.0.0
 */
global $xmas_advent_calendar_db_version;
$xmas_advent_calendar_db_version = '1.0';

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Xmas_Advent_Calendar
 * @subpackage Xmas_Advent_Calendar/includes
 * @author     Yowlu <info@yowlu.com>
 */
class Xmas_Advent_Calendar_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		// create the needed database table
		$creation_result = self::create_day_database_table();

		// if we just created the table, insert the default values
		if ($creation_result) {

			// insert the default values
			self::insert_to_day_database_table();

			// set default options
			self::set_default_options();

		}

	}

	/**
	 * Create the needed database table.
	 *
	 * To init our plugin we need to create a table that will store the options for each day.
	 *
	 * @since    1.0.0
	 */
	public function create_day_database_table() {

		// get the global wordpress database variable
		global $wpdb;
		
		// get the global xmas advent calendar version
		global $xmas_advent_calendar_db_version;

		// create a name for the table
   		$table_name = $wpdb->prefix . "xmas_advent_calendar_day";

   		// get the defined charset
   		$charset_collate = $wpdb->get_charset_collate();

   		// declare the query
		$sql =	"CREATE TABLE IF NOT EXISTS $table_name (
  				id mediumint(2) NOT NULL,
  				position mediumint(2) NOT NULL,
  				image varchar(256) NOT NULL,
  				modal_header longtext NOT NULL,
  				modal_code longtext NOT NULL,
  				update_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
  				UNIQUE KEY id (id)
				) $charset_collate;";

		// call the upgrade file where dbDelta is defined
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		// perform the query
		$creation_result = dbDelta( $sql );

		// add the current db version
		add_option( 'xmas_advent_calendar_db_version', $xmas_advent_calendar_db_version );

		// return the result
		return $creation_result;
	}

	/**
	 * Insert the default values.
	 *
	 * To continue the setup of our plugin we need to insert the default data to the table such as all the images or the placeholder texts.
	 *
	 * @since    1.0.0
	 */
	public function insert_to_day_database_table() {

		// get the global wordpress database variable
		global $wpdb;

		// create the default modal code to be inserted on our database
		$default_modal_code =	array(	/*'https://www.youtube.com/watch?v=ONyXSnL2LtA',*/ // for videos, install a lazy load plugin to avoid network issues
										'<h1 class="xmas-advent-calendar-title" style="text-align: center;">Xmas is coming!</h1>' .
										'<p class="xmas-advent-calendar-subtitle" style="text-align: center;">We wanna celebrate with you that xmas is almost here and so we are offering you this special gift. Go and download it!</p>' .
										'<br>' .
										'<br>' .
										'<p class="xmas-advent-calendar-call-to-action-wrapper" style="text-align: center;"><a target="_blank" download="xmas-gift" class="xmas-advent-calendar-call-to-action" href="' . plugins_url() . '/xmas-advent-calendar/public/images/photo-default.png">Download</a></p>',
										'<h1 class="xmas-advent-calendar-title" style="text-align: center;">Xmas is coming!</h1>' .
										'<p class="xmas-advent-calendar-subtitle" style="text-align: center;">We wanna celebrate with you that xmas is almost here and so we are offering you this special gift. Go and download it!</p>' .
										'<img class="size-medium wp-image-8 aligncenter" src="' . plugins_url() . '/xmas-advent-calendar/public/images/photo-default.png" alt="default" />'
										);

		// set the name for the table
		$table_name = $wpdb->prefix . 'xmas_advent_calendar_day';

		// create an array of positions so the images have the default order we want
		$image_position = array(1, 6, 19, 2, 7, 14, 15, 4, 8, 12, 9, 13, 23, 22, 10, 5, 3, 25, 21, 16, 17, 18, 20, 11, 24);

		// loop 25 times as we have 25 days
		for ($i = 1; $i < 26 ; $i++) { 
			
			$wpdb->replace( 
				
				$table_name, 
				
				array( 
				
					'id' => $i,
					'position' => $i,
					'image' => 'public/images/xmas-advent-calendar-image-' . $image_position[ $i - 1 ] . '.png',
					'modal_header' => 'public/images/xmas-advent-calendar-header-' . ( ( $i % 8 ) + 1 ) . '.png',
					'modal_code' => stripslashes( wp_filter_post_kses( addslashes( $default_modal_code[ ( $i % 2 ) ] ) ) ),
					'update_time' => current_time( 'mysql' ), 
				
				) 
			
			);
		
		}		

	}

	/**
	 * Set some default values needed for the correct display of the plugin.
	 *
	 * To init our plugin we need to store the start and end date of the calendar as well as the type of visualization.
	 *
	 * @since    1.0.0
	 */
	public function set_default_options() {

		update_option( 'xmas_advent_calendar_start_date', '2017-12-01' );
        update_option( 'xmas_advent_calendar_end_date', '2017-12-25' );
        update_option( 'xmas_advent_calendar_type', 'side_sticky' );
        update_option( 'xmas_advent_calendar_allow_past_days', 'true');
        update_option( 'xmas_advent_calendar_toggle', 'public/images/icon-advent-calendar-3.png' );
        update_option( 'xmas_advent_calendar_color', 'color-input-wrapper-6' );

	}

}
